# Vue test task

This repository contains navigation issue

### **Issue:**

Application doesn't react on browser navigation  

### **Reproduce steps:**
- clear site data (cookie, cache, local storage, etc.)
- got to root page (http://localhost:8080)
- Sign in to application
- navigate through several inner pages (2-3 times)
- try to go back using browser back button or application `Go back` button
- after page reload all works well

### Goal
- fix the issue
- explain the reasons of buggy behaviour (what exactly goes wrong in low programming level)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
