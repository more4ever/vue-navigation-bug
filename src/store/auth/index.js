import authGetters from './getters';
import authMutations from './mutations';
import authState from './state';
import authActions from "./actions";

const authModule = {
    namespaced: true,
    actions: authActions,
    getters: authGetters,
    mutations:authMutations,
    state: authState,
};

export default authModule;
