const authMutations = {
    setUserData(state, {user}){
        if(user){
            localStorage.setItem('user', JSON.stringify(user));
        }else {
            localStorage.removeItem('user');
        }
        state.user = user;
    }
}

export default authMutations;
