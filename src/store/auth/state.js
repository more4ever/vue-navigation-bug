const initialUser =  localStorage.getItem('user');

const authState = {
    user: initialUser ? JSON.parse(initialUser) : undefined,
};

export default authState;
