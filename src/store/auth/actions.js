import authService from "@/services/authService";

const authActions = {
    async signIn({commit}, payload) {
        const userInfo = await authService.signIn(payload);

        commit('setUserData', {user: userInfo});
    },

    signOut({commit}) {
        commit('setUserData', {user: undefined})
    }
}

export default authActions;
