const authGetters = {
    isLoggedIn: (state) => {
        return Boolean(state.user)
    },
};

export default authGetters;
