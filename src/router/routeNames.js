const routesNames = {
    home: 'home',
    login: 'login',
    inner1: 'inner1',
    inner2: 'inner2',
    inner3: 'inner3',
}

export default routesNames;
