import VueRouter from "vue-router";
import store from "@/store";
import Vue from "vue";
import routes from "@/router/routes";
import routeNames from "@/router/routeNames";

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach(async (to, from, next) => {
    const matchedMeta = to.matched.map(record => record.meta);
    const auth = matchedMeta.some(meta => meta.auth);
    const guest = matchedMeta.some(meta => meta.guest);

    const isLoggedIn = store.getters['auth/isLoggedIn']

    if (auth && !isLoggedIn) {
        await router.push({name: routeNames.login})
        return;
    }

    if (guest && isLoggedIn) {
        await router.push({name: routeNames.home})
        return;
    }

    next();
})

Vue.use(VueRouter);

export default router;
