import routesNames from "@/router/routeNames";
import Login from "@/Login";
import Home from "@/pages/Home";
import InnerPage from "@/pages/InnerPage";

const PassThroughComponent = {
    render: (h) => h('router-view'),
};

const routes = [
    {path: '/login', name: routesNames.login, component: Login, meta: {guest: true}},
    {
        path: '/',
        meta: {auth: true},
        component: PassThroughComponent,
        children: [
            {
                path: '',
                name: routesNames.home,
                component: Home,

            },
            {
                path: '/inner1',
                name: routesNames.inner1,
                component: InnerPage,
            },
            {
                path: '/inner2',
                name: routesNames.inner2,
                component: InnerPage,
            },
            {
                path: '/inner3',
                name: routesNames.inner3,
                component: InnerPage,
            },
        ]
    }
]

export default routes
